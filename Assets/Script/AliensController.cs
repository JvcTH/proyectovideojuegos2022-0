using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AliensController : MonoBehaviour
{
    private Rigidbody2D rb;
    private Animator animator;
    private SpriteRenderer sr;
    public float velocity  = -3;
    public int vidas = 3;
    
    private GameController _game;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();
        
        _game = FindObjectOfType<GameController>();
        rb.velocity = new Vector2(velocity, rb.velocity.y);
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = new Vector2(velocity, rb.velocity.y); 
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag("pared"))
        {
            
            sr.flipX = !sr.flipX;
            velocity = velocity * -1;
        }

        if (collider.gameObject.CompareTag("bullet"))
        {
            vidas = vidas - 1;
            if (vidas <= 0)
            {
                _game.DestroyEnemy();
                Destroy(this.gameObject);
            }
        }
        if (collider.gameObject.CompareTag("bulletCharge"))
        {
            vidas = vidas - 2;
            if (vidas <= 0)
            {
                _game.DestroyEnemy();
                Destroy(this.gameObject);
            }
        }
    }
    private void OnCollisionEnter2D(Collision2D other)
    {
         if (other.gameObject.CompareTag("Jugador"))
        {
            sr.flipX = !sr.flipX;
            velocity = velocity * -1;
        }
        if (other.gameObject.CompareTag("enemy"))
        {
            sr.flipX = !sr.flipX;
            velocity = velocity * -1;
        }
        
    }
}