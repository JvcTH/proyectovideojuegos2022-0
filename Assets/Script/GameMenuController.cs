using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMenuController : MonoBehaviour
{
    // Sta
    
    public void MenuJuego() {
        SceneManager.LoadScene("Nivel1");
    }
    public void MenuOp() {
        SceneManager.LoadScene("MenuOpciones");
    }
     public void MenuAtras() {
        SceneManager.LoadScene("MenuPrincipal");
    }
    public void OpcionDial(){
        SceneManager.LoadScene("Dialogo1");
    }
    public void DialOp(){
        SceneManager.LoadScene("Nivel2");
    }
    public void DialOp1(){
        SceneManager.LoadScene("Nivel3");

    }
    public void DialOp2(){
        SceneManager.LoadScene("Final");
    }

   

    public void Salirjuego()
    {
        Application.Quit();
        Debug.Log("Salir");
    }
}
