using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProyectilNodriza : MonoBehaviour
{
 
    private SpriteRenderer sr;
    public Rigidbody2D rb;
    public float velocidad; 
    public Transform player;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        Destroy(this.gameObject, 6);
    }

    // Update is called once per frame
    void Update()
    {
        if (sr.flipX) { rb.velocity = new Vector2(-6, rb.velocity.y);}

        /*Vector2 objetivo=new Vector3(player.position.x,player.position.y,player.position.z);
        Vector2 nuevaPos=Vector2.MoveTowards(rb.position,objetivo,velocidad*Time.deltaTime);
        rb.MovePosition(nuevaPos);*/

    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Jugador")) {Destroy(this.gameObject);}
        
    }
}
