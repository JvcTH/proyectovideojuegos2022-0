using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

using UnityEngine.SceneManagement;


public class GameController : MonoBehaviour
{
    public Text enemysText;
    public Text lifesText;
    public Text timeText;
    public Text tipo1Text;
    
    private string disparosPrefsName="Disparos";
    private RobotController _robot;
   

    private int _enemys = 0;
    private int _lifes = 6;
    private float _time = 240;

    
    public int _tipo1 = 0;
    public int _tipo2 = 0;
    
    
    private void Start()
    { 
        
        lifesText.text = "Vidas: " + _lifes;
        tipo1Text.text = "Disparos: " + _tipo1;
           LoadData();                                                                                     
       
    }

    void Update()
    
    {
        
        if (timeText!=null)
        {
            _time -= Time.deltaTime;
            timeText.text = "Tiempo: " + _time;
        }
        if (_time <= 0)
        {
            SceneManager.LoadScene("Nivel2");
        }
        
    }

    public int GetScore()
    {return _enemys;}

    public void DestroyEnemy()
    {
        _enemys -= 1;
    }
    public void LoseLife()
    {
        _lifes -= 1;
       lifesText.text = "Vidas: " + _lifes;
    }
   
   

    public int GetLifes()
    { return _lifes;}
    public int GetEnemys()
    {return _enemys;}
      
    public void PlusTipo1(int tipo1)
    {

        _tipo1+= tipo1;
        tipo1Text.text = "Disparos: " + _tipo1;
        _tipo2=_tipo1;
    }
   

    private void OnDestroy(){
        SaveData();
    }
    private void SaveData(){
       
        PlayerPrefs.SetInt(disparosPrefsName,_tipo1);
        PlayerPrefs.SetInt(disparosPrefsName,_tipo2);
    }
    private void LoadData(){
        
        _tipo1=PlayerPrefs.GetInt(disparosPrefsName,0);
        _tipo2=PlayerPrefs.GetInt(disparosPrefsName,0);

    }
  
}